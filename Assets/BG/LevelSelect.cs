﻿/* LevelSelect.cs
 *
 * Programmer: Daniel Unterholzner
 *
 * This script is responsible for controlling the level select menu
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour
{
    public void LoadLevel(int levelNum)
    {
        Cursor.visible = false;
        SceneManager.LoadScene(levelNum);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
