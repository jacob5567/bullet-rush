﻿/* MainMenu.cs
 *
 * Programmer: Ethan Lo
 *
 * This script is responsible for controlling the main menu
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class MainMenu : MonoBehaviour
{

    public void PlayGame()
    {
        // Scene Build Index:
        //  0 - Main Menu
        //  1 - Level Select Menu
        //  2 - First Level
        //  3 - Second Level

        // SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    private AudioSource menuMusic;

    void Start()
    {
        menuMusic = GameObject.Find("menuMusic").GetComponent<AudioSource>();
        PlayerPrefs.SetInt("Difficulty", 1);  // Set the difficulty to Normal whenever the game starts
    }

    private void Update()
    {


        /*
        Maybe move level select scene into main menu scene
        -so music can continue
        */

        if (menuMusic.isPlaying == true)
        {
            return;
        }
        else
        {
            menuMusic.Play();
        }

    }

}
