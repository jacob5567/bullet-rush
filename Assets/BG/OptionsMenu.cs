﻿/* OptionsMenu.cs
 *
 * Programmer: Race Nelson
 *
 * This script is responsible for controlling how the difficulty buttons work
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : MonoBehaviour
{
    public void SetDifficultyEasy()
    {
        PlayerPrefs.SetInt("Difficulty", 0); // Set the difficulty to Easy
    }

    public void SetDifficultyNormal()
    {
        PlayerPrefs.SetInt("Difficulty", 1);  // Set the difficulty to Normal
    }

    public void SetDifficultyHard()
    {
        PlayerPrefs.SetInt("Difficulty", 2);  // Set the difficulty to Hard
    }
}
