﻿/* PlayerScript.cs
 * Programmer: Race Nelson
 * This script controls the boss's health
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealthScript : MonoBehaviour
{
    // Health Variables
    [Header("Health Settings")]
    [Tooltip("How much health the boss starts with")]
    public int initialHealth; // How many hit points the boss starts with
    [Tooltip("The health bar that will be displaying the HP of this boss")]
    public GameObject healthbar; // The health bar that will be displaying the HP of this boss
    private int health; // How many hit points the boss currently has

    // Start is called before the first frame update
    void Start()
    {
        health = initialHealth; // Set the boss's health to the value defined in the Inspector
    }

    // Changes the boss's health by an amount instead of setting to a value
    // Pass in negative values to remove health, positive values to add health
    // Updates health bar whenever damage is taken
    // Calls Die() function when HP reaches 0
    public void UpdateHealth(int delta)
    {
        health += delta;
        healthbar.GetComponent<BossHP>().UpdateBars(health, initialHealth);

        if (health <= 0)
        {
            health = 0;
            Die();
        }
    }

    // Kills the boss
    public void Die()
    {
        Destroy(this.gameObject);
        healthbar.GetComponent<BossHP>().HideBars(); // Hide the boss HP bar after death
        GameObject.Find("FPSPlayer").GetComponent<PlayerScript>().WinGame(); // Have the player win the game
    }

    // Setter function for boss's health value
    public void SetHealth(int hp)
    {
        health = hp;
        if (health < 0) { health = 0; } // Health cannot be less than 0
        healthbar.GetComponent<BossHP>().UpdateBars(health, initialHealth);
    }

    // Getter function for boss's health value
    public int GetHealth()
    {
        return health;
    }
}
