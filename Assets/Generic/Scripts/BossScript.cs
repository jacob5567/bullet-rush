﻿/* BossScript.cs
 * Programmers: Race Nelson, Jacob Faulk
 * This script stores the elements that are needed by every boss
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{
    // creates a class used to store GameObjects so that they're not being constantly created and destroyed at runtime
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }
    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    public GameObject slowdown; // The slowdown GameObject the boss will use

    protected int difficulty; // What difficulty the player has selected

}
