﻿/* HintsScript.cs
 * Programmer: Race Nelson
 * This script stores hint messages to display on the death screen.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintsScript : MonoBehaviour
{
    private string[] hints;

    // Awake is called once when the object is created
    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        hints = new string[] {
            "You can press Shift to sprint!" ,
            "You can right-click to slow down time!",
            "Your shots deal less damage from far away!",
            "You can shoot through enemy attacks on Boss 1!",
            "You can shoot down projectiles on Boss 2!",
            "This game has three difficulty modes, try out the other ones from the options menu!",
            "Your gun has unlimited ammo, so focus only on dealing damage!",
            "You can press Escape or P to pause the game!",
            "You can press Shift to sprint!" ,
            "You can right-click to slow down time!",
            "Your shots deal less damage from far away!",
            "You can shoot through enemy attacks on Boss 1!",
            "You can shoot down projectiles on Boss 2!",
            "This game has three difficulty modes, try out the other ones from the options menu!",
            "Your gun has unlimited ammo, so focus only on dealing damage!",
            "You can press Escape or P to pause the game!",
            "Remember, licking doorknobs is illegal on other planets!"
        };
    }

    // Returns a random hint from the array defined above
    public string GetHint()
    {
        return hints[Random.Range(0, hints.Length)];
    }
}
