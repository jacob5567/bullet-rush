﻿/* BossOneScript.cs
 *
 * Programmers: Race Nelson, Jacob Faulk
 *
 * This script governs the boss's attributes and actions
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PhaseClass;

public class BossOneScript : BossScript
{
    public Transform[] firePoints;

    [Tooltip("How long to delay before firing first projectiles, in seconds")]
    public float initialFireDelay;

    private float yRotation = 0;
    private float nextFire = 0f;
    private float nextMode = 0f;

    private int numMortarsEasy = 3;
    private int numMortars = 6;
    private int numMortarsHard = 10;

    public float rotationRate;

    private int attackMode; // 0 for rotate attack, 1 for straight attack
    private AudioSource bubbleShoot;

    // Movement Variables
    [Header("Movement Settings")]
    [Tooltip("Radius of circle around origin that boss will move within")]
    public float movementRange; // Radius of circle around origin that boss will move within
    [Tooltip("How fast the boss moves")]
    public float moveSpeed; // How fast the boss moves
    [Tooltip("Average delay between boss movements (in seconds)")]
    public float movementDelay; // Average wait time between movements
    [Tooltip("Randomness range for boss movement delay (in seconds). Delay is centered on value given in Movement Delay")]
    public float delayRandom; // How much to randomize the movement
    private float nextMove = 0.0f; // Time the boss will move next
    private bool moving = false; // Whether or not the boss is currently moving
    private Vector3 origin; // Location around which boss will move
    private Vector3 destination = Vector3.zero; // Location boss is moving towards

    // Start is called before the first frame update
    void Start()
    {
        // Load the difficulty settings, default to Normal
        difficulty = PlayerPrefs.GetInt("Difficulty", 1);

        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                if (pool.tag == "Bubble")
                {
                    obj.GetComponent<BubbleScript>().slowdown = slowdown;
                }
                else if (pool.tag == "Mortar")
                {
                    obj.GetComponent<MortarScript>().slowdown = slowdown;
                }
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.tag, objectPool);
        }

        attackMode = 3;
        nextMode = slowdown.GetComponent<SlowdownScript>().time() + initialFireDelay; //PhaseClass.duration(attackMode, difficulty);
        nextMove = slowdown.GetComponent<SlowdownScript>().time() + initialFireDelay; //PhaseClass.duration(attackMode, difficulty);
        nextFire = slowdown.GetComponent<SlowdownScript>().time();

        origin = transform.position;

        bubbleShoot = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (attackMode)
        {
            case 0:
                rotate(rotationRate * slowdown.GetComponent<SlowdownScript>().timeFactor);
                if (slowdown.GetComponent<SlowdownScript>().time() > nextFire)
                {
                    fireBubble();
                    nextFire = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.fireRate(attackMode, difficulty);
                    bubbleShoot.Play();
                }
                if (slowdown.GetComponent<SlowdownScript>().time() > nextMode)
                {
                    attackMode = 1;
                    nextMode = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.duration(attackMode, difficulty);
                }
                break;
            case 1:
                if (slowdown.GetComponent<SlowdownScript>().time() > nextFire)
                {
                    fireBubble();
                    fireMortar();
                    nextFire = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.fireRate(attackMode, difficulty);
                    bubbleShoot.Play();
                }
                if (slowdown.GetComponent<SlowdownScript>().time() > nextMode)
                {
                    attackMode = 2;
                    nextMode = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.duration(attackMode, difficulty);
                }
                break;
            case 2:
                rotate(-rotationRate * slowdown.GetComponent<SlowdownScript>().timeFactor);
                if (slowdown.GetComponent<SlowdownScript>().time() > nextFire)
                {
                    fireBubble();
                    nextFire = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.fireRate(attackMode, difficulty);
                    bubbleShoot.Play();
                }
                if (slowdown.GetComponent<SlowdownScript>().time() > nextMode)
                {
                    attackMode = 0;
                    nextMode = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.duration(attackMode, difficulty);
                }
                break;
            case 3: // Initial start delay
                if(slowdown.GetComponent<SlowdownScript>().time() > nextMode)
                {
                    attackMode = 0;
                    nextFire = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.fireRate(attackMode, difficulty);
                    nextMode = slowdown.GetComponent<SlowdownScript>().time() + PhaseClass.duration(attackMode, difficulty);
                }
                break;
            default:
                // if (Input.GetButtonDown("Fire2"))
                // {
                //     fireBubble();
                // }
                break;
        }

        // If the boss is currently moving
        if (moving)
        {
            float step = moveSpeed * Time.deltaTime * slowdown.GetComponent<SlowdownScript>().timeFactor;
            transform.position = Vector3.MoveTowards(transform.position, destination, step);

            // If we are basically to where we wanted to get
            if (Vector3.Distance(transform.position, destination) < 0.01f)
            {
                moving = false; // Stop moving

                // If we set zero randomness, use the movementDelay directly
                if (delayRandom == 0.0f)
                    nextMove = slowdown.GetComponent<SlowdownScript>().time() + movementDelay;
                else // Otherwise, generate a random delay centered on the movementDelay
                {
                    float delay = Random.Range(movementDelay - (delayRandom / 2), movementDelay + (delayRandom / 2));
                    nextMove = slowdown.GetComponent<SlowdownScript>().time() + delay;
                }
            }
        }
        else if (slowdown.GetComponent<SlowdownScript>().time() > nextMove) // If the boss is currently stationary and is not in its movement delay
        {
            // Picks a random point within a circle of its origin with radius movementRange
            Vector2 dest = new Vector2(100.0f, 100.0f) + Random.insideUnitCircle * movementRange;
            destination = new Vector3(dest.x, transform.position.y, dest.y); // Converts Vector2 given above into Vector3

            moving = true; // Start moving
        }
    }

    private void rotate(float degrees)
    {
        yRotation += Time.deltaTime * degrees;
        transform.rotation = Quaternion.Euler(90, yRotation, 0);
    }

    public void fireBubble()
    {
        foreach (Transform t in firePoints)
        {
            GameObject objToSpawn = poolDictionary["Bubble"].Dequeue();
            objToSpawn.SetActive(true);
            objToSpawn.transform.position = t.position;
            objToSpawn.transform.rotation = t.rotation;
            poolDictionary["Bubble"].Enqueue(objToSpawn);
        }
    }

    public void fireMortar()
    {
        int number;
        switch (difficulty)
        {
            case 0:
                number = numMortarsEasy;
                break;
            case 1:
                number = numMortars;
                break;
            case 2:
                number = numMortarsHard;
                break;
            default:
                number = numMortars;
                break;
        }
        for (int i = 0; i < number; i++)
        {
            GameObject objToSpawn = poolDictionary["Mortar"].Dequeue();
            objToSpawn.SetActive(true);
            objToSpawn.transform.position = transform.position;
            objToSpawn.transform.rotation = Quaternion.Euler(transform.up);
            poolDictionary["Mortar"].Enqueue(objToSpawn);
        }
    }
}
