﻿/*
 * BubbleScript.cs
 * Created by Jacob Faulk
 * This script controls the movement of the straight-firing bubbles
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleScript : MonoBehaviour
{
    public float speed = 30f;
    public Rigidbody rb;
    public GameObject popEffect;
    public GameObject slowdown;

    void Start()
    {
        rb.velocity = -transform.forward * speed;
    }

    void Update()
    {
        rb.velocity = -transform.forward * speed * slowdown.GetComponent<SlowdownScript>().timeFactor;
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.transform.tag != "Boss" && hit.transform.tag != "BossBullet")
        {
            if(hit.transform.tag == "PlayerCollider")
            {
                hit.gameObject.GetComponent<PlayerScript>().damage(1);
            }
            destroySelf();
        }
    }

    void destroySelf()
    {
        ParticleSystem pop = popEffect.GetComponent<ParticleSystem>();
        pop.Play();
        StartCoroutine(DisableSelf(pop.main.duration));
    }

    IEnumerator DisableSelf(float duration)
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive(false);
    }
}
