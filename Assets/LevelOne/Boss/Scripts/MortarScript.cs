﻿/*
 * MortarScript.cs
 * Created by Jacob Faulk
 * This script controls the movement of the mortar bubbles
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortarScript : MonoBehaviour
{

    public float initialSpeed = 30f;
    public float fallGravity = -10f;
    public Rigidbody rb;
    public GameObject popEffect;
    public GameObject slowdown;

    void OnEnable()
    {
        rb.velocity = new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(0.7f, 1f), Random.Range(-0.5f, 0.5f)) * initialSpeed * slowdown.GetComponent<SlowdownScript>().timeFactor;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            rb.velocity /= 4;
        }
        if(Input.GetButtonUp("Fire2"))
        {
                rb.velocity *= 4;
        }
        rb.AddForce(new Vector3(0, fallGravity, 0) * slowdown.GetComponent<SlowdownScript>().timeFactor * Time.timeScale, ForceMode.Acceleration);
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.transform.tag != "Boss" && hit.transform.tag != "BossBullet")
        {
            if (hit.transform.tag == "PlayerCollider")
            {
                hit.gameObject.GetComponent<PlayerScript>().damage(1);
            }
            destroySelf();
        }
    }

    void destroySelf()
    {
        ParticleSystem pop = popEffect.GetComponent<ParticleSystem>();
        pop.Play();
        StartCoroutine(DisableSelf(pop.main.duration));
    }

    IEnumerator DisableSelf(float duration)
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive(false);
    }
}
