/* PhaseClass.cs
 *
 * Programmers: Jacob Faulk
 *
 * A utility class that specifies the various boss phases for TestBoss.
 */

using System.Collections;
using System.Collections.Generic;

public class PhaseClass
{
    private static int numPhases = 4;
    private static float[] fireRatesEasy = new float[] { 0.2f, 0.2f, 0.2f, 0f }; // The fire rates for each phase
    private static float[] durationsEasy = new float[] { 7f, 7f, 7f, 5f }; // The durations for each phase (phase 3 is initial delay)
    private static float[] fireRates = new float[] { 0.1f, 0.05f, 0.1f, 0f }; // The fire rates for each phase
    private static float[] durations = new float[] { 7f, 7f, 7f, 5f }; // The durations for each phase (phase 3 is initial delay)
    private static float[] fireRatesHard = new float[] { 0.07f, 0.05f, 0.07f, 0f }; // The fire rates for each phase on hard difficulty
    private static float[] durationsHard = new float[] { 3f, 3f, 3f, 5f }; // The durations for each phase (phase 3 is initial delay) on hard difficulty

    public static float fireRate(int index, int difficulty)
    {
        if (index > numPhases)
        {
            throw new System.ArgumentException("Not a valid phase number: ", "index");
        }
        switch (difficulty)
        {
            case 0:
                return fireRatesEasy[index];
            case 1:
                return fireRates[index];
            case 2:
                return fireRatesHard[index];
            default:
                return fireRates[index];
        }
    }

    public static float duration(int index, int difficulty)
    {
        if (index > numPhases)
        {
            throw new System.ArgumentException("Not a valid phase number: ", "index");
        }
        switch (difficulty)
        {
            case 0:
                return durationsEasy[index];
            case 1:
                return durations[index];
            case 2:
                return durationsHard[index];
            default:
                return durations[index];
        }
    }
}
