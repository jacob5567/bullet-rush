﻿/* BossTwoScript.cs
 *
 * Programmer: Race Nelson
 *
 * This script is responsible for the actions and behaviors of the second boss
 */

using System.Collections.Generic;
using UnityEngine;

public class BossTwoScript : BossScript
{
    private SlowdownScript slowscript;

    public GameObject player;

    [Tooltip("How long to delay before firing first projectiles, in seconds")]
    public float initialFireDelay;

    [Header("Laser Settings")]
    [Tooltip("How wide to make the harmless guide laser")]
    public float laserGuideWidth;
    [Tooltip("How wide to make the harmful attack laser")]
    public float laserBeamWidth;
    [Tooltip("What color the guide laser should be")]
    public Color laserGuideColor;
    [Tooltip("What color the attack laser should be")]
    public Color laserBeamColor;
    [Tooltip("How long to wait before switching from guide to attack mode for lasers on Normal difficulty (in seconds)")]
    public float laserDelay;
    [Tooltip("How long to leave the attacking laser active on Normal difficulty (in seconds)")]
    public float laserDuration;
    [Tooltip("How long to wait before spawning a new laser on Normal difficulty (in seconds)")]
    public float laserAttackRate;
    [Tooltip("How long to wait before switching from guide to attack mode for lasers on Hard difficulty (in seconds)")]
    public float laserDelayHard;
    [Tooltip("How long to leave the attacking laser active on Hard difficulty (in seconds)")]
    public float laserDurationHard;
    [Tooltip("How long to wait before spawning a new laser on Hard difficulty (in seconds)")]
    public float laserAttackRateHard;

    private float nextLaser = 0.0f;

    [Header("Projectile Settings")]
    [Tooltip("How much health should the projectiles have on Normal difficulty?")]
    public int projectileHealth;
    [Tooltip("How fast should the projectiles move at base on Normal difficulty?")]
    public float projectileBaseSpeed;
    [Tooltip("How fast should the projectiles move at their fastest on Normal difficulty?")]
    public float projectileMaxSpeed;
    [Tooltip("How often should projectiles be spawned on Normal difficulty? (in seconds)")]
    public float projectileAttackRate;
    [Tooltip("How much health should the projectiles have on Hard difficulty?")]
    public int projectileHealthHard;
    [Tooltip("How fast should the projectiles move at base on Hard difficulty?")]
    public float projectileBaseSpeedHard;
    [Tooltip("How fast should the projectiles move at their fastest on Hard difficulty?")]
    public float projectileMaxSpeedHard;
    [Tooltip("How often should projectiles be spawned on Hard difficulty? (in seconds)")]
    public float projectileAttackRateHard;

    [Header("Sound Effects")]
    [Tooltip("Sound to play when tracking laser fires")]
    public AudioSource trackFire;
    // Source: http://soundbible.com/1669-Robot-Blip-2.html
    [Tooltip("Sound to play when damage laser fires")]
    public AudioSource realFire;
    // Source: https://freesound.org/people/bubaproducer/sounds/151022/
    [Tooltip("Sound to play when firing projectile")]
    public AudioSource projectileFire;
    // Source: http://soundbible.com/709-Bottle-Rocket.html

    private float nextProjectile = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        // Load the difficulty settings, default to Normal
        difficulty = PlayerPrefs.GetInt("Difficulty", 1);

        // Change the variables for the boss based on difficulty
        LoadDifficulty(difficulty);

        slowscript = slowdown.GetComponent<SlowdownScript>();

        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        // Initialize all of the GameObjects for the projectiles and lasers that will be used so it's not done every time something is needed
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            if (pool.tag == "Laser")
            {
                for (int i = 0; i < pool.size; i++)
                {
                    GameObject obj = Instantiate(pool.prefab); // Create object
                    obj.GetComponent<LaserScript>().Initialize(laserGuideWidth, laserBeamWidth, laserDelay,
                        laserDuration, laserGuideColor, laserBeamColor, slowdown); // Initialize variables
                    obj.SetActive(false); // Start inactive
                    objectPool.Enqueue(obj); // Add to queue
                }
            }
            else if (pool.tag == "Projectile")
            {
                for (int i = 0; i < pool.size; i++)
                {
                    GameObject obj = Instantiate(pool.prefab); // Create object
                    obj.GetComponent<ProjectileScript>().Initialize(projectileHealth, projectileBaseSpeed, slowdown, player); // Initialize variables
                    obj.SetActive(false); // Start inactive
                    objectPool.Enqueue(obj); // Add to queue
                }
            }
            poolDictionary.Add(pool.tag, objectPool); // Add to the pool for the object type
        }

        // The boss will wait a bit before the first attack
        nextLaser = slowscript.time() + laserAttackRate + initialFireDelay;
        nextProjectile = slowscript.time() + projectileAttackRate + initialFireDelay;
    }

    // Update is called once per frame
    void Update()
    {
        // If it's time to fire another laser
        if(slowscript.time() > nextLaser)
        {
            SpawnLaser(); // Create a new laser

            // Set the time to fire the next laser
            nextLaser = slowscript.time() + laserAttackRate;
        }

        if (slowscript.time() > nextProjectile)
        {
            SpawnProjectile(); // Create a new projectile

            // Randomly set the time to fire the next projectile
            // Random delay is in a range between 0.5 and 1.5 times the set value for delay
            float random = Random.Range(-1.0f, 1.0f);
            float delay = projectileAttackRate + (0.5f * projectileAttackRate * random);
            nextProjectile = slowscript.time() + delay;
        }
    }

    // Creates a laser object
    private void SpawnLaser()
    {
        // store the current HP percentage of the boss
        int health = this.gameObject.GetComponent<BossHealthScript>().GetHealth();
        int hpmax = this.gameObject.GetComponent<BossHealthScript>().initialHealth;
        float hpPct = ((float)health / (float)hpmax);

        int numLasers = 1;

        if (hpPct <= 0.667f)
            numLasers = 2;
        if (hpPct <= 0.333f)
            numLasers = 4;

        GameObject objToSpawn = poolDictionary["Laser"].Dequeue(); // Pops the first laser object off the queue

        Vector3 target = Vector3.zero; // Initialize blank vector

        // If on hard mode, the lasers will try to "lead" the player
        if (difficulty == 0)
        {
            target = player.transform.position;
        }
        else
        {
            // Replicates the player's current movement algorithm to try to "lead" the player
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            float playerSpeed = player.GetComponent<PlayerScript>().GetCurrentSpeed();
            Vector3 playerMovement = (player.transform.right * x) + (player.transform.forward * z);
            playerMovement *= playerSpeed;

            // Calculate where the player should be after moving next
            target = player.transform.position + playerMovement;
            // print(player.transform.position + " " + target);
        }

        // Initializes and starts the laser
        objToSpawn.GetComponent<LaserScript>().SetVerticies(transform.position, player.GetComponent<Transform>().position);
        objToSpawn.GetComponent<LaserScript>().LoadSounds(trackFire, realFire, true); // Load sound effects
        objToSpawn.GetComponent<LaserScript>().StartGuide();
        objToSpawn.SetActive(true); // Sets the laser object to "active"

        poolDictionary["Laser"].Enqueue(objToSpawn); // Adds the object back onto the queue to be re-used if needed later

        // Spawn a second laser when the boss gets below 66%
        if (numLasers > 1)
        {
            objToSpawn = poolDictionary["Laser"].Dequeue(); // Pops the first laser object off the queue

            // Initializes and starts the laser (second laser fires 180 degrees away from player)
            Vector3 heading = target - transform.position;
            target = transform.position - heading;

            objToSpawn.GetComponent<LaserScript>().SetVerticies(transform.position, target); // Set target
            objToSpawn.GetComponent<LaserScript>().LoadSounds(trackFire, realFire, false); // Tell laser to not use sounds
            objToSpawn.GetComponent<LaserScript>().StartGuide();

            objToSpawn.SetActive(true); // Sets the laser object to "active"

            poolDictionary["Laser"].Enqueue(objToSpawn); // Adds the object back onto the queue to be re-used if needed later

            // Spawn two *more* lasers when the boss gets below 33%
            if (numLasers > 2)
            {
                objToSpawn = poolDictionary["Laser"].Dequeue(); // Pops the first laser object off the queue
                GameObject obj2 = poolDictionary["Laser"].Dequeue(); // Pops the second off the stack

                // Sets the new lasers to fire towards the points 90 degrees away from the first two lasers
                heading = -Vector3.Cross(heading, Vector3.up);
                target = transform.position - heading;
                Vector3 target2 = transform.position + heading;

                objToSpawn.GetComponent<LaserScript>().SetVerticies(transform.position, target);
                objToSpawn.GetComponent<LaserScript>().LoadSounds(trackFire, realFire, false); // Tell laser to not use sounds
                objToSpawn.GetComponent<LaserScript>().StartGuide();
                objToSpawn.SetActive(true); // Sets the laser object to "active"
                poolDictionary["Laser"].Enqueue(objToSpawn); // Adds the object back onto the queue to be re-used if needed later

                obj2.GetComponent<LaserScript>().SetVerticies(transform.position, target2);
                obj2.GetComponent<LaserScript>().LoadSounds(trackFire, realFire, false); // Tell laser to not use sounds
                obj2.GetComponent<LaserScript>().StartGuide();
                obj2.SetActive(true); // Sets the laser object to "active"
                poolDictionary["Laser"].Enqueue(obj2); // Adds the object back onto the queue to be re-used if needed later
            }
        }
    }

    // Creates a projectile object
    private void SpawnProjectile()
    {
        projectileFire.Play(); // Play sound effect

        // Makes the projectiles move faster the lower health the boss is
        int health = this.gameObject.GetComponent<BossHealthScript>().GetHealth();
        int hpmax = this.gameObject.GetComponent<BossHealthScript>().initialHealth;
        float speed = projectileMaxSpeed;
        speed -= (projectileMaxSpeed - projectileBaseSpeed) * ((float)health / (float)hpmax);

        GameObject objToSpawn = poolDictionary["Projectile"].Dequeue(); // Pops the first projectile object off the queue

        // Sets the new projectile to start at the top center of the boss
        Vector3 pos = new Vector3(transform.position.x, transform.position.y + transform.lossyScale.y / 2, transform.position.z);
        objToSpawn.GetComponent<ProjectileScript>().SetStartConds(projectileHealth, speed, pos);
        objToSpawn.SetActive(true);

        poolDictionary["Projectile"].Enqueue(objToSpawn); // Adds the object back onto the queue to be re-used if needed later
    }

    // Changes the difficulty of the boss based on the difficulty the player selected on the menu
    public void LoadDifficulty(int diff)
    {
        // If the difficulty is set to easy or hard, change all of the variables
        if (diff == 0)
        {
            // Easy difficulty was implemented late, so the values are just hardcoded instead of integrated into the Inspector
            laserDelay = 1.5f;
            laserDuration = 2.5f;
            laserAttackRate = 0.75f;
            projectileHealth = 50;
            projectileBaseSpeed = 15;
            projectileMaxSpeed = 17.5f;
            projectileAttackRate = 2.5f;
        }
        else if (diff > 1)
        {
            laserDelay = laserDelayHard;
            laserDuration = laserDurationHard;
            laserAttackRate = laserAttackRateHard;
            projectileHealth = projectileHealthHard;
            projectileBaseSpeed = projectileBaseSpeedHard;
            projectileMaxSpeed = projectileMaxSpeedHard;
            projectileAttackRate = projectileAttackRateHard;
        }

    }
}
