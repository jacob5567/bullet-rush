﻿/* LaserScript.cs
 *
 * Programmer: Race Nelson
 *
 * This script is responsible controlling the lasers that the second boss fires
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    // easier access to necessary components
    private SlowdownScript slowscript;
    private LineRenderer linerend;
    private CapsuleCollider capsule;

    // variables to control the positioning and style of the laser
    private float guideWidth;
    private float beamWidth;
    private float delay;
    private float duration;
    private Vector3 startPos;
    private Vector3 endPos;
    private Color guideColor;
    private Color beamColor;

    // tracks which modes the laser is in
    private bool guideOn = false;
    private bool beamOn = false;

    // time the laser will change states
    private float changeTime = 0.0f;

    // Sounds
    private AudioSource trackSound;
    private AudioSource realSound;
    private bool isMainLaser;

    // Update is called once per frame
    void Update()
    {
        // If the guide laser is on
        if (guideOn)
        {
            // If it's time to change the laser to attack mode
            if (slowscript.time() >= changeTime)
            {
                guideOn = false; // leave guide mode

                StartBeam(); // call the function that starts attack beam mode
            }
        }
        else if (beamOn) // If it's in attack beam mode
        {
            if (slowscript.time() >= changeTime) // If it's time to leave attack beam mode
            {
                beamOn = false;
                DisableSelf(); // turn self off
            }
        }
    }

    // Acts like a constructor, setting variables and references
    public void Initialize(float gwidth, float bwidth, float wait, float dur, Color gcolor, Color bcolor, GameObject slowdown)
    {
        guideWidth = gwidth;
        beamWidth = bwidth;
        delay = wait;
        duration = dur;
        guideColor = gcolor;
        beamColor = bcolor;

        slowscript = slowdown.GetComponent<SlowdownScript>();

        linerend = this.gameObject.GetComponent<LineRenderer>();
        linerend.enabled = false;

        capsule = this.gameObject.GetComponent<CapsuleCollider>();
        capsule.enabled = false;
    }

    // Sets the start and end points for the LineRenderer
    public void SetVerticies(Vector3 start, Vector3 end)
    {
        startPos = start;

        Vector3 direction = (end - start) / (end - start).magnitude;

        endPos = new Vector3(start.x + (direction.x * 100), end.y, start.z + (direction.z * 100));

        linerend.SetPositions(new Vector3[2] { startPos, endPos });
    }

    // Loads in the sound effects and whether or not to play them
    public void LoadSounds(AudioSource track, AudioSource real, bool isMain)
    {
        trackSound = track;
        realSound = real;
        isMainLaser = isMain;
    }

    // Sets the variables for Guide Mode
    public void StartGuide()
    {
        if(isMainLaser)
            trackSound.Play(); // Play sound effect if this is the primary laser

        linerend.startColor = guideColor;
        linerend.endColor = guideColor;

        linerend.startWidth = guideWidth;
        linerend.endWidth = guideWidth;

        linerend.enabled = true;
        guideOn = true;

        changeTime = slowscript.time() + delay; // Set the time until it changes to attack mode
    }

    // Set the variables for Attack Beam Mode and initialize the collider
    private void StartBeam()
    {
        if (isMainLaser)
            if(realSound != null)
                realSound.Play(); // Play sound effect if this is the primary laser

        beamOn = true;
        linerend.startColor = beamColor;
        linerend.endColor = beamColor;

        // Increases size of beam in attack mode
        linerend.startWidth = beamWidth;
        linerend.endWidth = beamWidth;

        // Sets the dimensions of the collider to be on top of the laser
        capsule.radius = beamWidth / 2;
        capsule.direction = 2; // Z-axis

        Vector3 newPos = startPos - ((startPos - endPos) / 2);
        capsule.center = Vector3.zero;
        capsule.transform.position = newPos;

        capsule.transform.LookAt(startPos);
        capsule.height = 100;
        capsule.isTrigger = true;
        capsule.enabled = true;

        changeTime = slowscript.time() + duration; // set the laser disappearance time
    }

    // Disable the line renderer and the collider
    private void DisableSelf()
    {
        linerend.enabled = false;
        capsule.enabled = false;
    }

    // Detect a collision and damage the player
    private void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "PlayerCollider")
        {
            hit.gameObject.GetComponent<PlayerScript>().damage(1);
        }
    }
}
