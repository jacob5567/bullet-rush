﻿/* ProjectileScript.cs
 *
 * Programmer: Race Nelson
 *
 * This script is responsible controlling the projectiles that the second boss fires
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    // easier access to necessary components
    private SlowdownScript slowscript;
    private GameObject player;
    private Rigidbody rb;
    public GameObject explosion;

    // self-explanitory variables
    private int health;
    private float speed;

    // If the projectile is in its "ascent" or "chase" phase
    private bool phaseOne = true;

    // Load in variables on creation (acts like a constructor)
    public void Initialize(int hp, float sp, GameObject slowdown, GameObject p)
    {
        health = hp;
        speed = sp;

        slowscript = slowdown.GetComponent<SlowdownScript>();
        player = p;
        rb = this.gameObject.GetComponent<Rigidbody>();
    }

    // void OnEnable()
    // {
    //
    // }

    // Update variables when it's time for the object to actually enter the world
    public void SetStartConds(int hp, float sp, Vector3 pos)
    {
        transform.position = pos; // Sets start position
        health = hp;
        speed = sp; // Sets speed again (in case value changed in boss)

        phaseOne = true; // reset this variable

        // Remove all native velocity
        rb.velocity = Vector3.zero;

        // Reset size of warhead on top if it's changed
        foreach (Transform child in transform)
        {
            if (child.gameObject.name == "Sphere")
                child.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // easier access
        Vector3 pos = transform.position;

        // Do this when the object is first created
        if (phaseOne)
        {
            // transform.Find("Sphere").gameObject.SetActive(true);
            // transform.Find("Cylinder").gameObject.SetActive(true);
            transform.LookAt(new Vector3(transform.position.x, 10000, transform.position.z)); // Look straight up

            // Move straight up
            transform.position = new Vector3(pos.x, pos.y + (speed * 2 * slowscript.timeFactor * Time.deltaTime), pos.z);

            // If we've gone this far up, change mode
            if (transform.position.y >= 25.0f)
                phaseOne = false;
        }
        else // If we're in "chase mode"
        {
            // Attempt to "lead" the player
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            Vector3 playerMovement = (player.transform.right * x) + (player.transform.forward * z);
            playerMovement *= player.GetComponent<PlayerScript>().GetCurrentSpeed() * Time.deltaTime;

            // Calculate where the player should be next movement step
            Vector3 target = player.GetComponent<Transform>().position + playerMovement;

            // Actually move based on the math done above
            Vector3 heading = target - transform.position;
            Vector3 direction = heading / heading.magnitude; // normalized
            Vector3 movement = direction * speed * slowscript.timeFactor * Time.deltaTime;

            transform.position += movement;

            // makes projectile always face the target
            transform.LookAt(target);

        }

        // Remove all native velocity
        rb.velocity = Vector3.zero;
    }

    // When the player collides with a projectile
    private void OnTriggerEnter(Collider hit)
    {

        if (hit.gameObject.tag == "PlayerCollider")
        {
            // Debug.Log(hit.tag);
            hit.gameObject.GetComponent<PlayerScript>().damage(1);
            transform.position = new Vector3(10000, 0, 10000); // Move this to the middle of nowhere
            this.enabled = false; // Deactivate this object
        }
    }

    // When the player shoots the projectile
    public void ReduceHealth(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Vector3 tempLoc = transform.TransformPoint(0, 0, 0);
            transform.position = new Vector3(10000, 0, 10000); // Move this to the middle of nowhere
            explosion.transform.position = tempLoc;
            ParticleSystem e = Instantiate(explosion, tempLoc, Quaternion.identity).GetComponent<ParticleSystem>();
            e.Play();
            StartCoroutine(DisableSelf(e.main.duration));
        }

        // Reduce the size of the warhead on top when hit
        foreach (Transform child in transform)
        {
            if (child.gameObject.name == "Sphere")
                child.localScale *= 0.90f;
        }
        
    }

    IEnumerator DisableSelf(float duration)
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive(false);
    }
}
