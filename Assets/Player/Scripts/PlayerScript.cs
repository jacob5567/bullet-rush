﻿/* PlayerScript.cs
 * Made by Jacob Faulk, Race Nelson
 * This script controls the player's movement and other controls
 */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    [Header("Movement Settings")]
    [Tooltip("Character controller for the player")]
    public CharacterController controller;
    [Tooltip("Empty GameObject used for ground collision")]
    public Transform groundCheck; // Empty GameObject used to check whether the player is touching the ground or not
    [Tooltip("Distance between groundCheck and ground for the player to be considered \"on the ground\"")]
    public float groundDistance = 0.05f; // distance between groundCheck and ground for the player to be considered "on the ground"
    [Tooltip("Layer to be checked as \"ground\"")]
    public LayerMask groundMask; // The layer to be checked as ground"
    private bool isGrounded;

    [Tooltip("How high the player can jump")]
    public float jumpHeight = 4f;

    [Tooltip("Speed player moves normally")]
    public float moveSpeed = 12f;
    [Tooltip("Speed player moves while sprinting")]
    public float sprintSpeed = 24f;
    private float currentSpeed = 12f;
    [Tooltip("Force of gravity on the player")]
    public float fallGravity = -20f; // force of gravity on the player
    Vector3 velocity;
    bool sprinting = false;


    [Header("Health settings")]
    [Tooltip("How many hit points the player has")]
    public int maxHealth = 3;
    private int health;
    [Tooltip("How long to wait before the player can respawn")]
    public float deathDelay = 1.0f;
    private float respawnMinTime;
    [Tooltip("How long to wait before returning to the menu after victory")]
    public float victoryDelay = 5.0f;
    private float victoryTime;

    [Tooltip("What UI element is used to display the player's health")]
    public Transform playerHealthUI;
    [Tooltip("What prefab to use as the health indicator for the player")]
    public GameObject healthIndicatorPrefab;

    [Header("Slowdown settings")]
    [Tooltip("GameObject controlling the slowdown effect")]
    public GameObject slowdown;
    private float stamina = 100.0f;
    private float staminaRechargeStartTime = 0.0f;
    [Tooltip("UI element that displays the player's stamina amount")]
    public GameObject staminaBar;
    [Tooltip("Rate at which the stamina bar decreases, per second")]
    public float staminaDecreaseRate = 10.0f;
    [Tooltip("Rate at which the stamina bar refills, per second")]
    public float staminaRechargeRate = 5.0f;
    [Tooltip("How long, in seconds, to wait before beginning to refill stamina")]
    public float staminaMinDowntime = 5.0f; // How long before stamina begins to recharge, in seconds

    private bool slowdownOn = false;

    // Sound effects
    public AudioSource hit1;
    public AudioSource hit2;
    public AudioSource hit3;
    public AudioSource deathSound;
    public AudioSource slowMo_IN;
    public AudioSource slowMo_OUT;

    // Booleans that track game status
    private bool alive = true;
    private bool victory = false;

    void Start()
    {
        health = maxHealth;
        for (int i = 0; i < health; i++)
        {
            GameObject healthImg = Instantiate(healthIndicatorPrefab, new Vector3((55 * i) - 45, 5, 0), Quaternion.identity);
            healthImg.transform.SetParent(playerHealthUI, false);
        }

        // jumpSound = GetComponent<AudioSource>();
        // hit1 = GameObject.Find("randHit1").GetComponent<AudioSource>();
        // hit2 = GameObject.Find("randHit2").GetComponent<AudioSource>();
        // hit3 = GameObject.Find("randHit3").GetComponent<AudioSource>();
        // deathSound = GameObject.Find("death").GetComponent<AudioSource>();
        // slowMo_IN = GameObject.Find("slowMo_IN").GetComponent<AudioSource>();
        // slowMo_OUT = GameObject.Find("slowMo_OUT").GetComponent<AudioSource>();
    }

    void Update()
    {
        // Only let the player do things when they're alive and not paused
        if (alive && Time.timeScale > 0.0f)
        {
            // Plays slowdown off sound if right click is released while stamina is remaining
            if (Input.GetButtonUp("Fire2") && slowdownOn)
            {
                slowMo_OUT.Play();
                slowdownOn = false;
            }

            // check for slowdown or speedup
            if (Input.GetButton("Fire2"))
            {

                if (stamina > 0)
                {
                    // play start of stamina sound
                    if (!slowdownOn)
                    {
                        slowdownOn = true;
                        slowMo_IN.Play();
                    }
                    staminaRechargeStartTime = Time.time + staminaMinDowntime; // Starts a 5 second countdown before stamina will recharge

                    slowdown.GetComponent<SlowdownScript>().slow();
                    stamina -= staminaDecreaseRate * Time.deltaTime;

                    // Prevents stamina from going below 0%
                    if (stamina < 0)
                        stamina = 0.0f;

                    staminaBar.GetComponent<StaminaBar>().UpdateBars(stamina); // Update the stamina bar
                }
                else
                {
                    slowdown.GetComponent<SlowdownScript>().normal();

                    // play out of stamina sound
                    if (slowdownOn)
                    {
                        slowMo_OUT.Play();
                        slowdownOn = false;
                    }
                }

            }
            else
            {
                slowdownOn = false;
                slowdown.GetComponent<SlowdownScript>().normal(); // Play a sound when returning to normal speed
            }

            if (Time.time >= staminaRechargeStartTime) // If stamina is currently being allowed to recharge
            {
                if (stamina < 100)
                {
                    stamina += staminaRechargeRate * Time.deltaTime;
                    staminaBar.GetComponent<StaminaBar>().UpdateBars(stamina); // Update the stamina bar
                }

                // Prevents stamina from going over 100%
                if (stamina > 100)
                {
                    stamina = 100.0f;
                    staminaBar.GetComponent<StaminaBar>().UpdateBars(stamina); // Update the stamina bar
                }
            }

            // Check if the player is on the ground
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            // If the player is on the ground, keep them there
            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            sprinting = Input.GetButton("Sprint");

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 move = (transform.right * x) + (transform.forward * z);

            // The player moves faster when sprinting
            if (sprinting)
            {
                currentSpeed = sprintSpeed;
            }
            else
            {
                currentSpeed = moveSpeed;
            }

            controller.Move(move * currentSpeed * Time.deltaTime); // Move the player

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                // jumpSound.Play();
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * fallGravity);
            }

            velocity.y += fallGravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
        }
        else if (Time.timeScale > 0.0f) // If the player is dead
        {
            if (Time.time >= respawnMinTime) // if the player is allowed to respawn
            {
                if (Input.anyKey) // if the user presses any button
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
            }
        }

        if (victory) // If the player has won
        {
            if (Time.time >= victoryTime) // If the victory delay time has passed
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                SceneManager.LoadScene(0); // Return to the menu
            }
        }
    }

    // Whenever the player takes damage
    public void damage(int damage)
    {
        if (!victory) // if the player hasn't won yet
        {
            health -= damage;

            if (health >= 0) // If we're still alive
            {
                randHitSound(); // Play a hit sound
                Transform maxChild = null;
                float maxCoordX = -10000;
                
                // Update UI to reflect loss of HP
                foreach (Transform c in playerHealthUI)
                {
                    if (c.gameObject.activeSelf)
                    {
                        if (c.position.x > maxCoordX)
                        {
                            maxChild = c;
                            maxCoordX = c.position.x;
                        }
                    }
                }
                if (maxChild != null)
                    maxChild.gameObject.SetActive(false);
            }
            if (health <= 0) // If we died
            {
                // Only do this once
                if (alive)
                {
                    deathSound.Play();
                    alive = false; // Set the player as dead
                    respawnMinTime = Time.time + deathDelay; // Set the time the player can respawn at

                    // Find the game object that contains the death message
                    GameObject deathMessage = GameObject.Find("Canvas").transform.Find("DeathMessage").gameObject;
                    deathMessage.SetActive(true); // Display death screen
                    
                    // Turn off all other UI elements
                    GameObject.Find("Stamina").SetActive(false);
                    GameObject.Find("Crosshair").SetActive(false);
                    GameObject.Find("BossHealth").SetActive(false);

                    // Find the hint text box and put a random hint in it
                    string hint = GameObject.Find("HintsObject").GetComponent<HintsScript>().GetHint();
                    GameObject.Find("HintText").GetComponent<Text>().text = hint;
                }
            }
        }
    }

    // Play a sound when the player gets hit
    public void randHitSound()
    {
        int num = Random.Range(1, 3);
        switch (num)
        {
            case 1:
                hit1.Play();
                break;
            case 2:
                hit2.Play();
                break;
            case 3:
                hit3.Play();
                break;
            default:
                break;
        }
    }

    // Return how much health the player has left
    public int getHealth()
    {
        return health;
    }

    // Return how much slowdown energy the player has left
    public float GetStamina()
    {
        return stamina;
    }

    // Sets stamina to value in range 0 to 100, inclusive
    public void SetStamina(float stam)
    {
        if (stam > 100.0f)
            stamina = 100.0f;
        else if (stam < 0.0f)
            stamina = 0.0f;
        else
            stamina = stam;

        staminaBar.GetComponent<StaminaBar>().UpdateBars(stamina); // Update the stamina bar
    }

    // Return how fast the player is currently moving
    public float GetCurrentSpeed()
    {
        return currentSpeed;
    }

    // Called by the boss when the player wins the game
    public void WinGame()
    {
        victory = true; // Set victory to true
        victoryTime = Time.time + victoryDelay; // Start the victory delay

        GameObject canvas = GameObject.Find("Canvas");
        canvas.transform.Find("WinMessage").gameObject.SetActive(true); // Display victory message
    }
}
