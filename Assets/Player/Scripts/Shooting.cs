﻿/* Shooting.cs
 *
 * Programmer: Race Nelson
 *
 * This script is responsible for the player's ability to shoot the boss.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [Tooltip("Maximum distance the player can hit the target from (in units)")]
    public float range; // Range at which the player can shoot enemies
    [Tooltip("Delay between shots (in seconds)")]
    public float fireRate; // How fast the player can deal damage to the boss, in seconds
    [Tooltip("How much damage to deal per shot")]
    public int damage; // How much damage to deal per shot
    [Tooltip("How far, in units, for each step of damage falloff")]
    public float falloffThreshold;
    [Tooltip("How much damage to fall off at each step")]
    public int falloffAmount = 1;

    private float nextFire = 0.0f; // When the player is next allowed to fire

    private Crosshair crosshair; // The player's crosshair
    private Camera cam; // The camera we control

    [Tooltip("Prefab for the effect to play on hit")]
    public GameObject hitEffectPrefab;
    private GameObject hitEffect;
    private AudioSource rifleShoot;

    [Tooltip("Location of the end of the gun barrel in 3-D space")]
    public Transform gunEnd;

    // Variables to control the shot trails
    private WaitForSeconds shotDuration = new WaitForSeconds(0.02f);
    private LineRenderer gunTrail;
    private LineRenderer trails;
    private Queue<LineRenderer> pool;
    private LineRenderer line;

    // Start is called before the first frame update
    private void Start()
    {
        hitEffect = Instantiate(hitEffectPrefab, transform.position, transform.rotation);
        crosshair = GameObject.Find("Crosshair").GetComponent<Crosshair>(); // Assign the crosshair variable to the crosshair's GameObject
        cam = GameObject.Find("PlayerCamera").GetComponent<Camera>();

        rifleShoot = GameObject.Find("Updated Gun Model").GetComponent<AudioSource>();

        gunTrail = GameObject.Find("Updated Gun Model").GetComponent<LineRenderer>();

        // Create a pool of LineRenderers so that we aren't constantly creating and destroying them at runtime
        pool = new Queue<LineRenderer>();
        for(int i=0; i<5; i++)
        {
            line = Instantiate(gunTrail);
            line.transform.parent = transform;
            line.enabled = false;
            line.startWidth = 0.1f;
            line.endWidth = 0.1f;
            pool.Enqueue(line);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale > 0.0f) // Do nothing if we're paused
        {
            // If the user is pressing the fire key and is able to fire a shot
            if (Input.GetButton("Fire1") && Time.time > nextFire)
            {
                crosshair.Decrease(); // Decrease the crosshair as long as we're shooting

                trails = pool.Dequeue();
                StartCoroutine(trailEffect(trails));


                // Initialize a ray for a raycast that starts at the player's position and heads where they are looking
                Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                trails.SetPosition(0, gunEnd.position);

                // If the raycast hits something within the range defined
                if (Physics.Raycast(ray, out RaycastHit hit, range))
                {
                    trails.SetPosition(1, hit.point);

                    // Store the tag of the object we collided with
                    string tag = hit.transform.tag;

                    // Checks what to do when we hit specific tags
                    switch (tag)
                    {
                        case "Boss": // If we are hitting the boss
                            int newDamage = damage;

                            // Calculate how far from the boss we are
                            float range = (hit.transform.position - transform.position).magnitude;
                            int rangeBracket = (int)Mathf.Floor(range / falloffThreshold); // Determine how much falloff to apply

                            // Remove falloffAmount from the damage to deal every falloffThreshold units
                            for (int i = 0; i < rangeBracket; i++)
                            {
                                newDamage -= falloffAmount;
                            }

                            // Deal the current weapon damage to the boss, after damage falloff
                            hit.transform.gameObject.GetComponent<BossHealthScript>().UpdateHealth(-newDamage);

                            crosshair.OpacityUp(); // Increase the opacity of the boss hit indicator

                            break;
                        case "Boss2Projectile": // If we are hitting one of the projectiles from the second boss
                                                // Deal damage to it (no damage falloff applied)
                            hit.transform.gameObject.GetComponent<ProjectileScript>().ReduceHealth(damage);
                            crosshair.OpacityUp(); // Increase the opacity of the boss hit indicator
                            break;
                        default:
                            // crosshair.OpacityDown(); // Decrease the opacity of the boss hit indicator if we hit something else
                            break;
                    }

                    //****Had to move this outside to make sound work***
                    nextFire = Time.time + fireRate;    // Starts the shot cooldown timer
                    rifleShoot.Play();
                    //**************************************

                    hitEffect.transform.position = hit.transform.position;
                    hitEffect.transform.rotation = Quaternion.FromToRotation(hit.transform.position, transform.position);
                    hitEffect.transform.rotation = hit.transform.rotation;
                    ParticleSystem effect = hitEffect.GetComponent<ParticleSystem>();
                    effect.Play();

                }
                else // If we don't hit anything
                {
                    trails.SetPosition(1, ray.origin + (cam.transform.forward * range));

                    crosshair.OpacityDown(); // Decrease the opacity of the boss hit indicator if we hit nothing

                    // Starts the shot cooldown timer
                    nextFire = Time.time + fireRate;
                    rifleShoot.Play();
                }
            }
            else if (Time.time <= nextFire) // If we are pressing the button but can't fire yet
            {
                // This space intentionally left blank
                // Don't modify the crosshair at all if we can't fire
            }
            else // If we aren't pressing the button
            {
                crosshair.Increase(); // Grow the crosshair if we aren't firing
                crosshair.OpacityZero(); // Remove the opacity of the boss hit indicator if we aren't firing
            }
        }
    }

    // Draws the line that simulates bullet trails
    private IEnumerator trailEffect (LineRenderer trails)
    {
        trails.enabled = true;
        yield return shotDuration;
        trails.enabled = false;
        pool.Enqueue(trails);
    }
}
