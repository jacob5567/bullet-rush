﻿/* SlowdownScript.cs
 *
 * Created by Jacob Faulk
 *
 * Controls the slowdown effect of the game.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowdownScript : MonoBehaviour
{
    private float normalSpeed = 1f;
    private float slowSpeed = 0.25f;
    private float alteredTime;
    public float timeFactor;

    void Start()
    {
        timeFactor = normalSpeed;
        alteredTime = Time.time;
    }

    void Update()
    {
        alteredTime += Time.deltaTime * timeFactor;
    }

    public void normal()
    {
        timeFactor = normalSpeed;
    }

    public void slow()
    {
        timeFactor = slowSpeed;
    }

    public float time()
    {
        return alteredTime;
    }

    public bool isSlow()
    {
        return timeFactor == slowSpeed;
    }
}
