﻿/* Crosshair.cs
 *
 * Programmer: Race Nelson
 *
 * This script is responsible for growing and shrinking the crosshair
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    private readonly float minSpacing = 11.0f; // How far apart each of the limbs are from the center at their minimum distance
    private readonly float maxSpacing = 23.0f; // How far apart the limbs are at their maximum extent
    [Tooltip("How fast the limbs should move when the gun is shooting (Default: 1.0)")]
    public float bloomSpeed = 1.0f; // How much the limbs should move out when the gun is shooting

    // Called when the player is shooting the boss
    public void Increase()
    {
        GameObject limbs = transform.Find("CrosshairLimbs").gameObject;

        // Moves each of the four limbs away from the center
        foreach (Transform child in limbs.GetComponent<Transform>())
        {
            RectTransform childRT = child.gameObject.GetComponent<RectTransform>();

            childRT.anchoredPosition = ChangeChildPos(childRT.anchoredPosition, 1);
        }
    }

    // Called when the player is *not* shooting the boss
    public void Decrease()
    {
        GameObject limbs = transform.Find("CrosshairLimbs").gameObject;

        // Moves each of the four limbs towards the center
        foreach (Transform child in limbs.GetComponent<Transform>())
        {
            RectTransform childRT = child.gameObject.GetComponent<RectTransform>();

            childRT.anchoredPosition = ChangeChildPos(childRT.anchoredPosition, -1);
        }
    }

    public void OpacityUp()
    {
        CanvasGroup diag = transform.Find("CrosshairDiag").gameObject.GetComponent<CanvasGroup>();

        if (diag.alpha < 1.0f)
            diag.alpha += 0.25f;
        else
            diag.alpha = 1.0f;
    }

    public void OpacityDown()
    {
        CanvasGroup diag = transform.Find("CrosshairDiag").gameObject.GetComponent<CanvasGroup>();

        if (diag.alpha > 0.0f)
            diag.alpha -= 0.25f;
        else
            diag.alpha = 0.0f;
    }

    public void OpacityZero()
    {
        CanvasGroup diag = transform.Find("CrosshairDiag").gameObject.GetComponent<CanvasGroup>();
        diag.alpha = 0.0f;
    }

    public void OpacityMax()
    {
        CanvasGroup diag = transform.Find("CrosshairDiag").gameObject.GetComponent<CanvasGroup>();
        diag.alpha = 1.0f;
    }

    // Does the math that moves the limbs.
    // mod is either +1 or -1 to indicate if the crosshair is growing or shrinking
    private Vector2 ChangeChildPos(Vector2 currentPos, int mod)
    {
        float delta = bloomSpeed * mod; // Changes bloomSpeed to be positive or negative

        Vector2 toReturn = new Vector2(0.0f, 0.0f);

        // Increases or decreases the spacing between the inclusive bounds ±minSpacing, ±maxSpacing
        if (currentPos.x == 0) // Top or bottom
        {
            if (currentPos.y > 0) // Top
            {
                toReturn.y = currentPos.y + delta;

                if (toReturn.y > maxSpacing)
                    toReturn.y = maxSpacing;
                else if (toReturn.y < minSpacing)
                    toReturn.y = minSpacing;
            }
            else // Bottom
            {
                toReturn.y = currentPos.y - delta;

                if (toReturn.y < -maxSpacing)
                    toReturn.y = -maxSpacing;
                else if (toReturn.y > -minSpacing)
                    toReturn.y = -minSpacing;
            }
        }
        else // Left or right
        {
            if (currentPos.x > 0) // Right
            {
                toReturn.x = currentPos.x + delta;

                if (toReturn.x > maxSpacing)
                    toReturn.x = maxSpacing;
                else if (toReturn.x < minSpacing)
                    toReturn.x = minSpacing;
            }
            else // Left
            { 
                toReturn.x = currentPos.x - delta;

                if (toReturn.x < -maxSpacing)
                    toReturn.x = -maxSpacing;
                else if (toReturn.x > -minSpacing)
                    toReturn.x = -minSpacing;
            }
        }

        return toReturn;
    }
}
