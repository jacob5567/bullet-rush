﻿/* PauseMenu.cs
 *
 * Programmers: Daniel Unterholzner, Race Nelson
 *
 * This script controls the ability to pause the game as well as what the pause buttons do
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#pragma warning disable 0414 // to disable the unused variable warnings

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenuUI;
    private bool pauseMenuEnabled;

    // Start is called before the first frame update
    void Start()
    {
        pauseMenuUI.SetActive(false);
        pauseMenuEnabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetButtonDown("Pause")) && (!pauseMenuEnabled))
        {
            EnablePauseMenu();
            // Debug.Log("Enabling the pause menu");
        }
        else if ((Input.GetButtonDown("Pause")) && (pauseMenuEnabled))
        {
            DisablePauseMenu();
            // Debug.Log("Disabling the pause menu");
        }
    }

    void EnablePauseMenu()
    {
        Cursor.visible = true;
        pauseMenuUI.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        pauseMenuEnabled = true;
        Time.timeScale = 0.0f;
    }

    void DisablePauseMenu()
    {
        pauseMenuUI.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        pauseMenuEnabled = false;
        Time.timeScale = 1.0f;
    }

    public void Resume()
    {
        DisablePauseMenu();
        // Debug.Log("Disabling the pause menu");
    }

    public void Menu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(0); // Return to the menu
        // Debug.Log("Returning to menu");
    }

    public void Quit()
    {
        // print("QUIT");
        Application.Quit(0);
    }
}
