﻿/* StaminaBar.cs
 *
 * Programmer: Race Nelson
 *
 * This script controls the player's slowdown stamina bar in the UI
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaBar : MonoBehaviour
{
    private RectTransform current;
    private RectTransform missing;

    private float barWidth;
    private float barHeight;

    // Start is called before the first frame update
    void Start()
    {
        // Store max size of the bar into variables for easy access
        barWidth = this.gameObject.GetComponent<RectTransform>().rect.width;
        barHeight = this.gameObject.GetComponent<RectTransform>().rect.height;

        // Initialize current and missing health bars
        current = transform.Find("CurrentStam").gameObject.GetComponent<RectTransform>();
        missing = transform.Find("MissingStam").gameObject.GetComponent<RectTransform>();

        // Set size of current health to full and size of missing health to empty
        current.sizeDelta = new Vector2(barWidth, barHeight);
        missing.sizeDelta = new Vector2(0, barHeight);
        // Debug.Log(missing.sizeDelta);

        // Sets the position of the bars
        current.anchoredPosition = new Vector2(-((barWidth / 2) - (current.rect.width / 2)), 0);
        missing.anchoredPosition = new Vector2((barWidth / 2) - (missing.rect.width / 2), 0);
    }

    // Update the size and position of the bars
    // Called whenever the player's stamina changes
    public void UpdateBars(float stam)
    {
        stam /= 100.0f; // Convert the float into the range 0 to 1 (from 0 to 100)

        // Update the size of the current and missing bar based on the player's current stamina percentage
        current.sizeDelta = new Vector2(barWidth * stam, barHeight);
        missing.sizeDelta = new Vector2(barWidth * (1.0f - stam), barHeight);

        if (missing.rect.width >= barWidth)
        {
            missing.sizeDelta = new Vector2(barWidth, barHeight);
        }

        // Update the position of the bars so they still combine to form a normal bar
        current.anchoredPosition = new Vector2(-((barWidth / 2) - (current.rect.width / 2)), 0);
        missing.anchoredPosition = new Vector2((barWidth / 2) - (missing.rect.width / 2), 0);
    }

    // Show the bars
    public void ShowBars()
    {
        current.gameObject.SetActive(true);
        missing.gameObject.SetActive(true);
    }

    // Hide the bars
    public void HideBars()
    {
        current.gameObject.SetActive(false);
        missing.gameObject.SetActive(false);
    }
}
